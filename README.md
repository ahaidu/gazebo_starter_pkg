# Gazebo starter package (gazebo_starter_pgk)
Various task examples in order to get familiar with Gazebo.

## Usage
```
$ mkdir build && cd build
$ cmake ..
$ make
```
### Task 1
Printing out the world name and physics engine type using a World Plugin.

### Usage Task 1

Used files:

 * `include/GetPhysEngine.hh`
 * `src/GetPhysEngine.cc`

```
$ gazebo -u --verbose worlds/get_phys_engine.world
```

### Task 2
Creating a joint between object1 and object2, with a 5 sec delay after object1 leaves ground

### Usage Task 2

Used files:

 * `include/GreateJoint.hh`
 * `src/CreateJoint.cc`

```
$ gazebo -u --verbose worlds/create_joint.world
```
### Task 3
CustomMsgPublisher using a World Plugin

### Usage Task 3

Used files:

 * `include/CustomMsgPublisher.hh`
 * `src/CustomMsgPublisher.cc`

```
$ gazebo -u --verbose worlds/custom_publisher.world
```
### Task 4
SystemPublisher using Vector3D example

### Usage Task 4

Used files:

 * `CMakeLists.txt`
 * `include/SystemPublisher.hh`
 * `src/CMakeLists.txt`
 * `src/SystemPublisher.cc`

```
$ gazebo --verbose
$ gz topic -l
$ gz topic -e /your_topic 
```
### Task 5
WorldPublisher using Vector3D example - similar to Task 4 using a world plugin

### Usage Task 5

Used files:

 * `CMakeLists.txt`
 * `include/WorldPublisher.hh`
 * `src/CMakeLists.txt`
 * `src/WorldPublisher.cc`
 * `src/WorldPublisher.cc`

```
$ gazebo world/ --verbose
```

### Task 6
Printing out "Hello World" using a World Plugin

### Usage Task 6

Used files:

 * `CMakeLists.txt`
 * `hello.world`
 * `include/HelloWorld.hh`
 * `src/CMakeLists.txt`
 * `src/HelloWorld.cc`

```
$ gazebo hello.world --verbose
```

### Task 7
Click on the play button in the gui to unpause the simulation, and you should see the box move.

### Usage Task 7

Used files:

 * `CMakeLists.txt`
 * `include/ModelPush.hh`
 * `model_push.world`
 * `src/CMakeLists.txt`
 * `src/ModelPush.cc`

```
$ gazebo -u model_push.world
```

### Task 8
The Gazebo window should show an environment with a sphere, box, and cylinder arranged in a row.

### Usage Task 8

Used files:

 * `factory.world`
 * `include/Factory.hh`
 * `models/box/model.config`
 * `models/box/model.sdf`
 * `models/cylinder/model.config`
 * `models/cylinder/model.sdf`
 * `src/CMakeLists.txt`
 * `src/Factory.cc`
 * `CMakeLists.txt`

```
$ gazebo factory.world
```

### Task 9
You should see an empty world.If you add a box to the world using the Box icon, the box should float up and away from the camera.

### Usage Task 9

Used files:

 * `CMakeLists.txt`
 * `include/WorldEdit.hh`
 * `src/CMakeLists.txt`
 * `src/WorldEdit.cc`
 * `world_edit.world`

```
$ gazebo world_edit.world
```

### Task 10
Inside /tmp/gazebo_frames you should see many saved images from the current plugin

### Usage Task 10

Used files:

 * `CMakeLists.txt`
 * `include/SystemGui.hh`
 * `src/CMakeLists.txt`
 * `src/SystemGui.cc`

```
$ gzserver &
$ gzclient -g libsystem_gui.so
```

### Task 11
MoveObj is simulating the chemistry instruments to check if spheres are getting out when the model is moving up-down, right-left and rotates.

### Usage Task 11

Used files:

 * `CMakeLists.txt`
 * `include/MoveObj.hh`
 * `src/CMakeLists.txt`
 * `src/MoveObj.cc`
 * `kithcen.world`

```
$ gazebo kithcen.world --verbose -u
```
