# Overview

This tutorial describes the process of creating a model using the 3D computer graphics software,  Blender.

## Opening Blender

*  Make sure Blender is [installed](https://www.blender.org/download/).

*  Start up Blender.

## Adding meshes

*  Delete all objects: click right on object and then press `Delete` key on your keyboard.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/empty_blender.png)


*  To add a custom mesh, use combination `Shift+A`, then select the target mesh. 


![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/blender_mesh.png)

    
## Measuring Cylinder

# Creating your model

*  To create a measuring cylinder you need to add a Cylinder Mesh.
*  From the top menu bar you have to change from `Blender Render ` to `Cycles Render`

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/cycles_render_blender.png)


*  On the left side you have the section `Add Cylinder` where you should see `Cap Fill Type`. Open the menu selections and select `Triangle Fan`. If you have an older version of Blender that does not have this option, then you can skip this step.

*  From the bottom menu bar open menu selections and change to `Edit Mode`, then open the menu selection from its right hand side and select `Wireframe`


![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/edit_mode_blender.png)

        
*  Right click on this top center point to select it and then press the `Delete` key on your keyboard and then click on `Vertices`. That will delete the top portion of this model.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/delete_vertices_blender.png)

*  Now you are going to change the view by pressing some keys on the number pad (in blender, the numbers on the right side number pad are different than the number on top of keyboard). Also, you can use `View` menu from the bottom menu bar and there you will find the slections that you can use instead of the number pad.
By pressing the number 1 on the number pad you are shown a front view and pressing 5 toggles between perspective and orthogonal mode. If you press number 5 on the number pad, it will switch to orthogonal mode. Using the combination `Shift+Middle click` you can move your camera left-right, top-down. Then you will use the scroll wheel to zoom in.

*  Put your cursor over the object and press `Ctrl+R` to control the subdivide loops command. With the cursor still over the object use the scroll wheel and scroll up and so you have more lines and then press the left mouse button. If you want to change the position of the lines, move the mouse up or down, then press `Enter`. In our case we need just 1 line.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/subdivide_blender.png)
        
*  An important key is the letter `A`. Pressing `A` toggles between selecting everything and turning all selections off. Now by ussing the `A` key we check if everything is deselected. Now we need to select just the top vertices, to do that press and release the `B` key and then press and hold the left mouse button while you dragg the selection box around the top verices. If you want to move these top vertices down or up, you can do that by left clicking on that blue arrow and then just dragg the arrow down or up.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/selection_vertices_blender.png)

*  In the next you can scale these verticies by pressing the `S` key and after that you can use your mouse to scale these.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/scale_blender.png)
          
*  Press `A` key to deselect everything and now press again `A` key to elect everything. After that press `S` and by using your mouse scale all verticies inside.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/scale_inside_blender.png)

*  In the next step, we are going to select the bottom verticies using the `B` key and then press and hold the left mouse button while you dragg the selection box around the top verices. After that press the `E` key, to extrude, then press `Enter` and then press again `E` and move the mouse to extrude down. By pressing `Z` you will extrude straight.
            
![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/extrude1_blender.png)

*  Now we are going to scale in the outside the verticies by pressing the `S` key and after that you can use your mouse to scale these

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/scale_bottom_blender.png)
         
* Press `E` key again to extrude downwards by using your mouse.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/extrude2_blender.png)
        
* Press `E` key, but now we are going to extrude to the same point, so press the `Enter` key now. Go to left side menu, you should see a button called `Merged`. Click on the `Merged` and select `At Center`. You can see that we merged all verticies to the center.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/merge1_blender.png)
        
* From the bottom menu bar open menu selections and change to `Object Mode` then open the menu selections from its right hand side and select `Solid`. 

* Go to the left side menu and you should see a button called `Smooth`. Click on it and now your object should look different.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/smooth_blender.png)
        
* On the right hand side you should see the `+` button. Click on it and now you have a menu from where you can perform some settings.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/transform_menu_blender.png)

* From the left menu change Units to `Metric`.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/metric_blender.png)
        
* Now we are going to set up the dimensions for our object, from the left menu, `Transform -> Dimensions` section.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/cec1b3c72590cc4fef1ae5c458ca8bafe5bca8a3/tutorials/blender_models/files/dimensions_blender.png)
        
* From the left menu, `3D Cursor -> Location` section, we are going to set up the location of the cursor in 0. Also from `Transform -> Location` we will set up the position of our object, or we `right click` on the object then with `G` key we move the object where we want. 

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/b0454ee245296a5d204fc087a017f42465ff7115/tutorials/blender_models/files/object_location_blender.png)

* Now we are setting the orgin to 3D curso by using combination `Ctrl+Shift+Alt+C` and select Origin to 3D Cursor.

# Saving your model

* We are going to export our model in gazebo: `File -> Export -> Collada(Default)(.dae)`.     

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/b0454ee245296a5d204fc087a017f42465ff7115/tutorials/blender_models/files/export_gazebo_blender.png)
         
* A dialog will come up where you cand chose the name and location for your model. Also on the left hand side you will see `Export COLLADA` menu, where you have to check `Selction Only`.

![Alt text](https://bytebucket.org/dobracristian/gazebo_starter_pkg/raw/b0454ee245296a5d204fc087a017f42465ff7115/tutorials/blender_models/files/selection_only_blender.png)