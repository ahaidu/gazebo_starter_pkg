#include "CorkSimulation.hh"
#include "GzUtils.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void CorkSimulation::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
	const math::Vector3 vector;

	//Read from SDF file
	const std::string model_name1 =sim_games::GetSDFValue(
			std::string("modelName1"), _sdf, std::string("default_name"));
	const std::string model_name2 =sim_games::GetSDFValue(
							std::string("modelName2"), _sdf, std::string("default_name"));
	const double break_limit = sim_games::GetSDFValue(
			std::string("break_limit"), _sdf, 0.1);

	// Store the pointer to the model
	this->world = _parent;
	this->object1 = this->world->GetModel(model_name1);
	this->object2 = this->world->GetModel(model_name2);
	this->break_limit = break_limit;
	this->engine = this->world->GetPhysicsEngine();

	this->object2->SetGravityMode(false);

	// Listen to the update event. This event is broadcast every
	// simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
  			boost::bind(&CorkSimulation::OnUpdate, this));

	//Create joint
	this->myJoint = this->engine->CreateJoint("revolute",
					this->object1);
	//Set the joint position
	this->jointPose = math::Pose();

	//Load the joint at set position
	this->myJoint->Load((this->object1->GetLinks()).at(0),
			(this->object2->GetLinks()).at(0), jointPose);

	this->myJoint->SetAxis(0, math::Vector3(0, 0, 1));

	//Attach the joint
	this->myJoint->Attach((this->object1->GetLinks()).at(0),
			(this->object2->GetLinks()).at(0));

	//Enable feedback provide
	this->myJoint->SetProvideFeedback(true);
	std::cout << "Joint attached successfully between " <<
			this->object1->GetName() << " and " <<
			this->object2->GetName() << std::endl;
}

//////////////////////////////////////////////////
void CorkSimulation::OnUpdate()
{
	float resulting_force;

	gazebo::physics::JointWrench testWrench;
	testWrench = myJoint->GetForceTorque(this->myJoint->GetId());

	//Resulting force
	resulting_force = testWrench.body2Force.z - testWrench.body1Force.z;
	std::cout << "Resuting force = " << resulting_force<< std::endl;

	//Break joint
	if (resulting_force > break_limit )
	{
		this->myJoint->Detach();
		this->object2->SetGravityMode(true);
		std::cout <<this->object1->GetName()<<
		" joint detached from " << this->object2->GetName() << std::endl;
	}
}
// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(CorkSimulation)
