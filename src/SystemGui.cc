#include "SystemGui.hh"

using namespace gazebo;

/////////////////////////////////////////////
    /// \brief Called after the plugin has been constructed.
    void SystemGUI::Load(int /*_argc*/, char ** /*_argv*/)
    {std::cout << "SystemGUI: Entering Load()" << std::endl;
      this->connections.push_back(
          event::Events::ConnectPreRender(
            boost::bind(&SystemGUI::Update, this)));
    }

/////////////////////////////////////////////
    // \brief Called once after Load
    void SystemGUI::Init()
    {
	std::cout << "SystemGUI: Entering Init()" << std::endl;
    }

/////////////////////////////////////////////
    /// \brief Called every PreRender event. See the Load function.
    void SystemGUI::Update()
    {	std::cout << "SystemGUI: Entering Update()" << std::endl;
      if (!this->userCam)
      {
        // Get a pointer to the active user camera
        this->userCam = gui::get_active_camera();

        // Enable saving frames
        this->userCam->EnableSaveFrame(true);

        // Specify the path to save frames into
        this->userCam->SetSaveFramePathname("/tmp/gazebo_frames");
      }

      // Get scene pointer
      rendering::ScenePtr scene = rendering::get_scene();

      // Wait until the scene is initialized.
      if (!scene || !scene->GetInitialized())
        return;

      // Look for a specific visual by name.
      if (scene->GetVisual("ground_plane"))
        std::cout << "Has ground plane visual\n";
	std::cout << "SystemGUI: Entering Update()" << std::endl;
    }

// Register this plugin with the simulator
GZ_REGISTER_SYSTEM_PLUGIN(SystemGUI)
