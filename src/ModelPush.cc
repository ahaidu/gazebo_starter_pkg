#include "ModelPush.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
{
  std::cout << "ModelPush: Entering Load()" << std::endl;
  // Store the pointer to the model
  this->model = _parent;

  // Listen to the update event. This event is broadcast every
  // simulation iteration.
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
	  boost::bind(&ModelPush::OnUpdate, this, _1));

}

//////////////////////////////////////////////////
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
{
  std::cout << "ModelPush: Entering OnUpdate()" << std::endl;
  // Apply a small linear velocity to the model.
  this->model->SetLinearVel(math::Vector3(.3, 0, 0));
}
 
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)
