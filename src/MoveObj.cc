#include "MoveObj.hh"
#include "GzUtils.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void MoveObj::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
	const std::string model_name =sim_games::GetSDFValue(
			std::string("modelName"), _sdf, std::string("default_name"));
	std::cout << "MoveObj: Entering Load()" << std::endl;

	// Store the pointer to the model
	this->world = _parent;
	this->object1 = world->GetModel(model_name);
	this->object1->SetGravityMode(false);

	// Listen to the update event. This event is broadcast every
	// simulation iteration.
	this->updateConnection = event::Events::ConnectWorldUpdateBegin(
  			boost::bind(&MoveObj::OnUpdate, this));

	this->prevWallTime = this->world->GetSimTime();

}

//////////////////////////////////////////////////
void MoveObj::OnUpdate()
{
	common::Time t = this->world->GetSimTime();
	std::cout << "MoveObj: Entering OnUpdate(): "<< std::endl;

	if ((t.sec - this->prevWallTime.sec) < 5 && (t.sec - this->prevWallTime.sec) >1)
	{
		std::cout <<"Move2"<< std::endl;
		this->object1->SetLinearVel(math::Vector3(0, 0, 0.2));
		this->object1->SetAngularVel(math::Vector3(0, 0, 0));

	}

	 if ((t.sec - this->prevWallTime.sec) >= 5 && (t.sec - this->prevWallTime.sec) <= 7)
	 {
		 std::cout <<"Move3"<< std::endl;
		 this->object1->SetLinearVel(math::Vector3(0.0, 1, 0));
		 this->object1->SetAngularVel(math::Vector3(0, 0, 0));
	 }

	 if ((t.sec - this->prevWallTime.sec) > 7 && (t.sec - this->prevWallTime.sec) <= 9)
	 {
		 std::cout <<"Move4"<< std::endl;
		 this->object1->SetLinearVel(math::Vector3(0.0, -1, 0));
		 this->object1->SetAngularVel(math::Vector3(0, 0, 0));
	 }

	 if ((t.sec - this->prevWallTime.sec) > 9 && (t.sec - this->prevWallTime.sec) <= 10)
	 {
		 std::cout <<"Move5"<< std::endl;
		 this->object1->SetLinearVel(math::Vector3(0.0, 1, 0));
		 this->object1->SetAngularVel(math::Vector3(0, 0, 0));
	 }

	 if ((t.sec - this->prevWallTime.sec) > 10 && (t.sec - this->prevWallTime.sec) <= 11)
	 {
		 std::cout <<"Move6"<< std::endl;
	 	 this->object1->SetLinearVel(math::Vector3(0.0, -1, 0));
	  	 this->object1->SetAngularVel(math::Vector3(0, 0, 0));
	 }

	 if ((t.sec - this->prevWallTime.sec) > 11 && (t.sec - this->prevWallTime.sec) < 33)
	 {
		 std::cout <<"Move7"<< std::endl;
		 this->object1->SetLinearVel(math::Vector3(0.0, 0, 0));
		 this->object1->SetAngularVel(math::Vector3(0.8, 0, 0.2));
	 }
}

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(MoveObj)
