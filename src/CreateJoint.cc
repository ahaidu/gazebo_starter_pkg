#include "CreateJoint.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void CreateJoint::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/) {
		std::cout << "CreateJoint: Entering Load()" << std::endl;
		this->world =_parent;
		std::cout << world->GetName() << std::endl;
		this->object1 = world->GetModel("object_1");
		this->object2 = world->GetModel("object_2");
		this->engine = world->GetPhysicsEngine();
		this->prevWallTime = common::Time::GetWallTime();
		this->flagMin = false;
		this->flagMax = false;
	}

//////////////////////////////////////////////////
void CreateJoint::Init() {
		std::cout << "CreateJoint: Entering Init()" << std::endl;
		this->loadWorld = event::Events::ConnectWorldUpdateBegin(
				boost::bind(&CreateJoint::OnUpdate, this));
	}

//////////////////////////////////////////////////
void CreateJoint::OnUpdate() {
		std::cout << "CreateJoint: Entering OnUpdate()" << std::endl;
		this->object2->SetLinearVel(math::Vector3(0, 0, 0.5));
		if ((common::Time::GetWallTime().sec - this->prevWallTime.sec) > 5
				&& !flagMin)
		{
			std::cout << "TIME DIFF LARGER THAN 4 SEC" << std::endl;

			this->myJoint = this->engine->CreateJoint("revolute",
					this->object1);
			this->jointPose = math::Pose(1, 1, 0, 0, 1, 0);
			this->myJoint->Load(this->object1->GetLink("ob_1_link"),
					this->object2->GetLink("ob_2_link"), this->jointPose);
			this->myJoint->Attach(this->object1->GetLink("ob_1_link"),
					this->object2->GetLink("ob_2_link"));
			std::cout << "Joint attached successfully!" << std::endl;
			this->object2->SetLinearVel(math::Vector3(0, 0, 0.5));
			this->flagMin = true;
		}

		if ((common::Time::GetWallTime().sec - this->prevWallTime.sec) > 15
				&& !flagMax) {
			std::cout << "TIME DIFF LARGER THAN 12 SEC" << std::endl;
			this->myJoint->Detach();
			std::cout << "Joint detached!";
			this->flagMax = true;
		}

	}

GZ_REGISTER_WORLD_PLUGIN(CreateJoint)