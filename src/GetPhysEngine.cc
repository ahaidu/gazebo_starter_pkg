#include "GetPhysEngine.hh"

///linking to the namespace gazebo from GetPhysEngine.hh
using namespace gazebo;

//////////////////////////////////////////////////
///The Load function sets up the world and the engine which were added as class variables in GetPhysEngine class
void GetPhysEngine::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
{
	//Printing out a message in terminal for checking if we are in Load function
	std::cout << "GetPhysEngine: In Load function" << std::endl;

	//Set up the world. Store the pointer _parent to the world
	this->world = _parent;

	//Set up the engine. Store the pointer world to the engine using function GetPhysEngine
	this->engine = this->world->GetPhysicsEngine();

	//Printing out world name returned by getter function World::GetName()
	std::cout << "World name : " << this->world->GetName() << std::endl;

	//Printing out engine type returned by getter function World::GetType()
	std::cout << "Physics engine type : " << this->engine->GetType() << std::endl;
}

GZ_REGISTER_WORLD_PLUGIN(GetPhysEngine)
