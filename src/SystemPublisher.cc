#include "SystemPublisher.hh"

using namespace gazebo;

//////////////////////////////////////////////////
void SystemPublisher::Load(int /*_argc*/, char ** /*_argv*/)
{
	std::cout << "SystemPublisher: Entering Load()" << std::endl;
	this->counter = 0;
	this->updateRate = common::Time(2, 0);
	this->prevUpdateTime = common::Time::GetWallTime();
}

//////////////////////////////////////////////////
void SystemPublisher::Init()
{
	std::cout << "SystemPublisher: Entering Init()" << std::endl;
	this->node = transport::NodePtr(new transport::Node());
	this->node->Init("sphere_publisher");
	this->pub = node->Advertise<gazebo::msgs::Vector3d>("~/sphere_topic");

 	std::cout << "waiting for connection .. " << std::endl;
 	this->pub->WaitForConnection();
 	std::cout << "waiting for connection  DONE.. " << std::endl;

	this->loadConnection = event::Events::ConnectWorldUpdateBegin(
			boost::bind(&SystemPublisher::OnUpdate, this));
}

//////////////////////////////////////////////////
void SystemPublisher::OnUpdate()
{
//	std::cout << "SystemPublisher: Entering OnUpdat ()" << std::endl;
	if (common::Time::GetWallTime() - this->prevUpdateTime < this->updateRate)
		return;

	this->vector = gazebo::math::Vector3(this->counter, 1, 1);
	gazebo::msgs::Set(&msg, vector);
	std::cout << " Publishing " << std::endl;
	pub->Publish(msg);
	this->counter = counter + 0.1;

	this->prevUpdateTime = common::Time::GetWallTime();
}

GZ_REGISTER_SYSTEM_PLUGIN(SystemPublisher)
