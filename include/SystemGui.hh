#ifndef _SYSTEM_GUI_HH
#define _SYSTEM_GUI_HH

#include "gazebo/math/Rand.hh"
#include "gazebo/gui/GuiIface.hh"
#include "gazebo/rendering/rendering.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
 { 

	class SystemGUI : public SystemPlugin
	{       

		/// \brief Destructor
   		 public: virtual ~SystemGUI()
  		  {
     			 this->connections.clear();
     			 if (this->userCam)
       			 this->userCam->EnableSaveFrame(false);
    			 this->userCam.reset();
  		  }

                /// \brief Called after the plugin has been constructed.
		public: void Load(int /*_argc*/, char ** /*_argv*/);

		/// \breif Called every PreRender event. See the Load function.
		private: void Update();

		/// \breif Called once after Load.
		private: void Init();

		/// \breif Pointer the user camera.
   		private: rendering::UserCameraPtr userCam;

  		/// \breif All the event connections.
   		private: std::vector<event::ConnectionPtr> connections;
   		

	};
 }
#endif
