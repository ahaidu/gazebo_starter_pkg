#ifndef _WORLD_PUBLISHER_HH
#define _WORLD_PUBLISHER_HH

#include "gazebo/gazebo.hh"
#include "gazebo/transport/TransportIface.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/gui/GuiIface.hh"

namespace gazebo {

	 /// \Class WorldPublisher WorldPublisher.hh
	/// \brief Class for publishing vector3d messages
	class WorldPublisher: public WorldPlugin
	{
		/// \brief Pointer to the update event connection.
		private: event::ConnectionPtr updateConnection;

 		/// \brief Pointer to the load event connection.
 		private: event::ConnectionPtr loadConnection;

 		/// \brief Transport node pointer.
		private: gazebo::transport::NodePtr node;

 		/// \brief Transport publisher pointer.
 		gazebo::transport::PublisherPtr pub;

 		/// \brief Math vector.
       		gazebo::math::Vector3 vector;

        	/// \brief Standard double vector message.
       		 gazebo::msgs::Vector3d msg;

 		/// \brief Time variable for throttling the update rate.
 		common::Time updateRate;

		/// \brief Time variable for saving wall time value.
		common::Time prevUpdateTime;

		/// \brief Integer variable.
		double counter;

		// \brief Standard Load.
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		// \brief Standard Init.
		public: void Init();

		// \brief Update function for publishing messages on a topic.
		public: void OnUpdate();
	};
}

#endif
