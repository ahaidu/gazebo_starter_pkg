#ifndef _CUSTOM_MSG_PUBLISHER_HH
#define _CUSTOM_MSG_PUBLISHER_HH

//The necessary headers we need
#include "gazebo/gazebo.hh"
#include "gazebo/transport/TransportIface.hh"
#include "gazebo/msgs/msgs.hh"

//Plugins must be in gazebo namespace
namespace gazebo {

	 /// \Class CustomMsgPublisher CustomMsgPublisher.hh
	 /// \brief Class for publishing vector3d messages using a World Plugin.(Each plugin must be inherit from a plugin type)
	class CustomMsgPublisher: public WorldPlugin
	{
		/// \brief Standard connection pointer.Pointer to the loadConnection
 		private: event::ConnectionPtr loadConnection;

 		/// \brief Transport node pointer.
		private: gazebo::transport::NodePtr node;

 		/// \brief Transport publisher pointer.
 		gazebo::transport::PublisherPtr pub;

 		/// \brief Math vector.
        gazebo::math::Vector3 vector;

 		/// \brief Standard double vector message.
		gazebo::msgs::Vector3d msg;

 		/// \brief Time variable for throttling the update rate.
 		common::Time updateRate;

		/// \brief Time variable for saving wall time value.
		common::Time prevUpdateTime;

		/// \brief Integer variable.
		double counter;

		// \brief The World Plugin's Load function which receives sdf and physics arguments
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

		// \brief The World Plugin's Init function
		public: void Init();

		// \brief The World Plugin's OnUpdate function
		public: void OnUpdate();
	};
}

#endif
