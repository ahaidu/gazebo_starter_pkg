#ifndef _MOVE_OBJ_HH
#define _MOVE_OBJ_HH

#include "boost/bind.hpp"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "stdio.h"

namespace gazebo
 {

	class MoveObj: public WorldPlugin
	{
		/// \brief Load
		public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

		/// \Called by the world update start event
		public: void OnUpdate();

		/// \Pointer to the model
		private: physics::WorldPtr world;

		/// \brief Standard physics onject1.
		public:physics::ModelPtr object1;

		/// \Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;

		/// \brief Math vector.
		gazebo::math::Vector3 vector;

		/// \brief A static zero time variable set to common::Time(0, 0).
		public:	common::Time prevWallTime;

	};
 }
#endif
